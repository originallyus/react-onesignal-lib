/**
 * Hook that exposes an interval, similar to setInterval.
 * @param callback The callback function
 * @param delay The delay
 */
declare function useInterval(callback: () => void, delay: number | null): void;
export default useInterval;
