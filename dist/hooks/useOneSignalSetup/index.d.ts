/**
 * Hook that waits for oneSignal initialization before executing the callback
 * useful for using setEmail and setExternalUserId without getting error
 * it uses setInterval to check if OneSignal is setup, calling the callback when it is
 *
 * @param callback the callback to be called when oneSignal is initialized.
 * @param pollingIntervalMs time between checks, null to disable.
 */
declare const useOneSignalSetup: (callback: () => void, pollingIntervalMs?: number | null) => void;
export default useOneSignalSetup;
