import { IOneSignal, OneSignalOptions, IOneSignalEvent, IOneSignalAutoPromptOptions } from './oneSignal.types';
/**
 * Gets the window OneSignal instance.
 */
export declare const getOneSignalInstance: () => IOneSignal | null;
/**
 * Object for manipulating OneSignal.
 */
declare const ReactOneSignal: {
    getOneSignalInstance: () => IOneSignal | null;
    initialize: (appId: string, options: OneSignalOptions, events?: IOneSignalEvent[]) => void;
    notificationPermission: () => string[] | null;
    getNotificationPermission: () => Promise<string>;
    registerForPushNotifications: () => Promise<any>;
    showSlidedownPrompt: (options?: IOneSignalAutoPromptOptions | undefined) => Promise<void>;
    showCategorySlidedown: (options?: IOneSignalAutoPromptOptions | undefined) => Promise<void>;
    isPushNotificationsEnabled: () => Promise<boolean>;
    isPushNotificationsSupported: () => boolean | null;
    setSubscription: (unmute: boolean) => Promise<any>;
    setEmail: (email: string) => Promise<string>;
    logoutEmail: () => Promise<void>;
    getEmailId: () => Promise<string>;
    getPlayerId: () => Promise<string>;
    setExternalUserId: (externalUserId: string | number) => Promise<void>;
    removeExternalUserId: () => Promise<void>;
    getExternalUserId: () => Promise<string>;
    sendTag: (key: string, val: string) => Promise<string>;
    sendTags: (keyValues: object) => Promise<string>;
};
export default ReactOneSignal;
