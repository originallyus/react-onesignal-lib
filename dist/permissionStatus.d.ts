/**
 * Defines possible values for permissions statuses.
 */
declare enum permissionStatusEnum {
    Default = "default",
    Granted = "granted",
    Denied = "denied"
}
export default permissionStatusEnum;
