'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var react = require('react');

/**
 * Defines possible values for permissions statuses.
 */
var permissionStatusEnum;
(function (permissionStatusEnum) {
    permissionStatusEnum["Default"] = "default";
    permissionStatusEnum["Granted"] = "granted";
    permissionStatusEnum["Denied"] = "denied";
})(permissionStatusEnum || (permissionStatusEnum = {}));
var permissionStatusEnum$1 = permissionStatusEnum;

var DEFAULT_BASE_SCRIPT_ID = 'react-onesignal-base';
var DEFAULT_MODULE_SCRIPT_ID = 'react-onesignal-module';
/**
 * Provides the OneSignal script to inject.
 */
var ONE_SIGNAL_SCRIPT_SRC = 'https://cdn.onesignal.com/sdks/OneSignalSDK.js';
/**
 * Error to be thrown when OneSignal is not setup correctly.
 */
var ONESIGNAL_NOT_SETUP_ERROR = 'OneSignal is not setup correctly.';
/**
 * Maps The Options Object Into A String For The Module Script
 * @param options the options object
 * @param indent
 */
var mapOptionsObject = function (options, indent) {
    if (indent === void 0) { indent = 0; }
    var TABS_LENGTH = 2;
    var result = '';
    var optionKeys = Object.keys(options || {});
    for (var index = 0; index < optionKeys.length; index += 1) {
        var key = optionKeys[index];
        var hasOwnProperty = Object.prototype.hasOwnProperty.call(options, key);
        if (!hasOwnProperty) {
            continue;
        }
        var option = options[key];
        // Functions are not supported, so we'll ignore them
        if (typeof option === 'function') {
            continue;
        }
        if (key.includes('.')) {
            key = "\"" + key + "\"";
        }
        result += new Array(TABS_LENGTH * indent + 1).join(' ') + key + ": ";
        switch (typeof option) {
            case 'object':
                result += "{\n" + mapOptionsObject(option, indent + 1) + new Array(4 * indent + 1).join(' ') + "}";
                break;
            case 'boolean':
            case 'number':
                result += option;
                break;
            default:
                result += "\"" + option + "\"";
                break;
        }
        result += ',\n';
    }
    return result;
};
/**
 * Take our object of OneSignal events and construct listeners.
 *
 * @param eventsArr Array of event/callback key/value pairs defined by IOneSignalEvent interface.
 * @return {string} Script snippet for injecting into the native OneSignal.push()  method.
 */
var buildEventListeners = function (eventsArr) {
    var returnStr = '';
    // Let's make sure we've got an array that isn't empty.
    if (Array.isArray(eventsArr) && eventsArr.length) {
        eventsArr.forEach(function (event) {
            event.listener = event.listener || 'on';
            returnStr += "OneSignal." + event.listener + "('" + event.event + "', " + event.callback + ");";
        });
    }
    return returnStr;
};
/**
 * Provides the module script content to inject.
 */
var getModuleScriptBody = function (appId, options, events) {
    if (options === void 0) { options = {}; }
    if (events === void 0) { events = []; }
    var mappedOptions = mapOptionsObject(options);
    var listeners = buildEventListeners(events);
    return "\n    var OneSignal = window.OneSignal || [];\n    OneSignal.push(function() {\n      " + listeners + "\n      OneSignal.init({\n        appId: \"" + appId + "\",\n        " + mappedOptions + "\n      });\n    });\n  ";
};
/**
 * Gets the window OneSignal instance.
 */
var getOneSignalInstance = function () {
    var OneSignal = window['OneSignal'];
    if (OneSignal === null || OneSignal === void 0 ? void 0 : OneSignal.initialized) {
        return OneSignal;
    }
    return null;
};
/**
 * Injects one script into the DOM.
 * @param id script id.
 * @param buildScript script factory.
 */
var injectScript = function (id, buildScript) {
    var hasScript = !!document.getElementById(id);
    if (hasScript) {
        return;
    }
    var script = document.createElement('script');
    script.id = id;
    script = buildScript(script);
    document.body.appendChild(script);
};
/**
 * Injects the base script for OneSignal
 */
var injectBaseScript = function () {
    injectScript(DEFAULT_BASE_SCRIPT_ID, function (script) {
        script.src = ONE_SIGNAL_SCRIPT_SRC;
        return script;
    });
};
/**
 * Injects the module script for OneSignal
 */
var injectModuleScript = function (appId, options, events) {
    if (options === void 0) { options = {}; }
    if (events === void 0) { events = []; }
    injectScript(DEFAULT_MODULE_SCRIPT_ID, function (script) {
        script.innerHTML = getModuleScriptBody(appId, options, events);
        script.async = true;
        return script;
    });
};
/**
 * Initializes OneSignal.
 */
var initialize = function (appId, options, events) {
    if (events === void 0) { events = []; }
    if (!appId) {
        throw new Error('You need to provide your OneSignal appId.');
    }
    if (!document) {
        return;
    }
    injectBaseScript();
    injectModuleScript(appId, options, events);
};
/**
 * Array with every possible notification permission state.
 */
var notificationPermission = function () {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        return null;
    }
    return oneSignal.notificationPermission;
};
/**
 * Gets the current notification permission state.
 */
var getNotificationPermission = function () { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.getNotificationPermission()
            .then(function (value) { return resolve(value); })
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Attempt to register for push notifications.
 * If the user hasn't authorized push notifications yet,
 * this will show a prompt to do so.
 */
var registerForPushNotifications = function () { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.registerForPushNotifications()
            .then(function (value) { return resolve(value); })
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Shows a sliding modal prompt on the page for users.
 */
var showSlidedownPrompt = function (options) { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.showSlidedownPrompt(options)
            .then((function (value) { return resolve(value); }))
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Shows a category sliding modal prompt on the page for users.
 */
var showCategorySlidedown = function (options) { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.showCategorySlidedown(options)
            .then((function (value) { return resolve(value); }))
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Check if the user has already accepted push notifications and
 * successfully registered with Google's FCM server and OneSignal's
 * server (i.e. the user is able to receive notifications).
 * Only compatible with HTTPS
 *
 * @return {Promise<boolean>} A promise that return if the user is
 * able to receive notifications
 */
var isPushNotificationsEnabled = function () { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject();
        return;
    }
    try {
        oneSignal.isPushNotificationsEnabled()
            .then((function (value) { return resolve(value); }))
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Check if the current browser environment viewing the page
 * supports push notifications.
 *
 * @return {boolean} The current browser environment viewing the page
 * supports push notifications.
 */
var isPushNotificationsSupported = function () {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        return null;
    }
    return oneSignal.isPushNotificationsSupported();
};
/**
 * This function lets a site mute or unmute notifications for the current user.
 *
 * @param {boolean} unmute
 */
var setSubscription = function (unmute) { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.setSubscription(unmute)
            .then(function (value) { return resolve(value); })
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Sets the email on OneSignal instance.
 * @param email email
 */
var setEmail = function (email) { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.setEmail(email)
            .then(function (value) { return resolve(value); })
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Remove email on OneSignal instance.
 */
var logoutEmail = function () { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.logoutEmail()
            .then(function (value) { return resolve(value); })
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Gets the email ID configured on OneSignal instance.
 */
var getEmailId = function () { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.getEmailId()
            .then(function (value) { return resolve(value); })
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Returns the Player ID from this browser.
 */
var getPlayerId = function () { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.getUserId()
            .then(function (value) { return resolve(value); })
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Sets the external user ID on OneSignal instance.
 * @param externalUserId The external user ID
 */
var setExternalUserId = function (externalUserId) { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.setExternalUserId(externalUserId)
            .then(function () { return resolve(); })
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Sets the external user ID on OneSignal instance.
 */
var removeExternalUserId = function () { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.removeExternalUserId()
            .then(function () { return resolve(); })
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Gets the external user ID configured on OneSignal instance.
 */
var getExternalUserId = function () { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.getExternalUserId()
            .then(function (value) { return resolve(value); })
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Sets a key/value "tag" pair on OneSignal.
 *
 * @param key string
 * @param val string
 */
var sendTag = function (key, val) { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.sendTag(key, val)
            .then(function (value) { return resolve(value); })
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Sets a collection of key/value "tag" pairs on OneSignal.
 *
 * @param keyValues obj
 */
var sendTags = function (keyValues) { return new Promise(function (resolve, reject) {
    var oneSignal = getOneSignalInstance();
    if (!oneSignal) {
        reject(new Error(ONESIGNAL_NOT_SETUP_ERROR));
        return;
    }
    try {
        oneSignal.sendTags(keyValues)
            .then(function (value) { return resolve(value); })
            .catch(function (error) { return reject(error); });
    }
    catch (error) {
        reject(error);
    }
}); };
/**
 * Object for manipulating OneSignal.
 */
var ReactOneSignal = {
    getOneSignalInstance: getOneSignalInstance,
    initialize: initialize,
    notificationPermission: notificationPermission,
    getNotificationPermission: getNotificationPermission,
    registerForPushNotifications: registerForPushNotifications,
    showSlidedownPrompt: showSlidedownPrompt,
    showCategorySlidedown: showCategorySlidedown,
    isPushNotificationsEnabled: isPushNotificationsEnabled,
    isPushNotificationsSupported: isPushNotificationsSupported,
    setSubscription: setSubscription,
    setEmail: setEmail,
    logoutEmail: logoutEmail,
    getEmailId: getEmailId,
    getPlayerId: getPlayerId,
    setExternalUserId: setExternalUserId,
    removeExternalUserId: removeExternalUserId,
    getExternalUserId: getExternalUserId,
    sendTag: sendTag,
    sendTags: sendTags,
};

/**
 * Hook that exposes an interval, similar to setInterval.
 * @param callback The callback function
 * @param delay The delay
 */
function useInterval(callback, delay) {
    var savedCallback = react.useRef();
    // Remember the latest callback.
    react.useEffect(function () {
        savedCallback.current = callback;
    }, [callback]);
    // Set up the interval.
    react.useEffect(function () {
        if (delay !== null && savedCallback.current) {
            var tick = function () {
                savedCallback.current();
            };
            var id_1 = setInterval(tick, delay);
            return function () { return clearInterval(id_1); };
        }
        return function () { };
    }, [delay]);
}

/**
 * Hook that waits for oneSignal initialization before executing the callback
 * useful for using setEmail and setExternalUserId without getting error
 * it uses setInterval to check if OneSignal is setup, calling the callback when it is
 *
 * @param callback the callback to be called when oneSignal is initialized.
 * @param pollingIntervalMs time between checks, null to disable.
 */
var useOneSignalSetup = function (callback, pollingIntervalMs) {
    if (pollingIntervalMs === void 0) { pollingIntervalMs = 100; }
    var _a = react.useState(false), initialized = _a[0], setInitialized = _a[1];
    useInterval(function () {
        var oneSignal = getOneSignalInstance();
        if (oneSignal) {
            setInitialized(true);
            callback();
        }
    }, initialized ? null : pollingIntervalMs);
};

exports.default = ReactOneSignal;
exports.permissionStatus = permissionStatusEnum$1;
exports.useOneSignalSetup = useOneSignalSetup;
//# sourceMappingURL=index.js.map
